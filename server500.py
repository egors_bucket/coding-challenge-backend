﻿from flask import Flask, redirect, url_for, session, request, jsonify
from flask_oauthlib.client import OAuth

import urllib.request
from urllib.parse import unquote
import json


github_ClientID = 'c018d5a7d44230d11950'
github_ClientSecret = '4f5e4a0210099f21f695f2cd1425c13eb39e03ad'

FILTER_FIELDS = ('full_name', 'html_url', 'description', 'stargazers_count', 'language')
GITHUB_REQUEST = 'https://api.github.com/search/repositories?q=+language:{}&sort=stars:>{}&order=asc&page={}&per_page={}'

app = Flask(__name__)
app.secret_key = 'secret_key'

oauth = OAuth(app)
github = oauth.remote_app(
    'github',
    consumer_key=github_ClientID,
    consumer_secret=github_ClientSecret,
    request_token_params={},
    base_url='https://api.github.com/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://github.com/login/oauth/access_token',
    authorize_url='https://github.com/login/oauth/authorize'
)


def get_next_link(links):
    linki = links.split(', ')
    link1_2 = linki[0].strip('<')
    link2_2 = linki[0].strip('<')
    link1 = link1_2.split('>; rel=')
    link2 = link2_2.split('>; rel=')
    if 'next' in link1[1]:
        return unquote(link1[0])
    else:
        return None


def filter_row(info):
    return {k: v for k, v in info.items() if k in FILTER_FIELDS}


@app.route('/task')
def req_to_github_api():
    stars = 500
    page = 1
    per_page = 100  # max 100
    _out = ()

    if not 'github_token' in session:
        print(dir(github))
        return jsonify({'Error': 'Session does not exists.'}), 401

    req_str = GITHUB_REQUEST.format('python', stars, page, per_page)
    req = urllib.request.Request(req_str)
    if 'github_token' in session:
        req.add_header('Authorization', 'Bearer {}'.format(session['github_token'][0]))
    else:
        return jsonify({'Error': 'Error GitHub authorization.'}), 401

    try:
        print(req.headers, req.full_url)
        res = urllib.request.urlopen(req)
    except Exception as e:
        print(e)
        return jsonify({'Error': 'Error occured while calling urlopen. '}), 400

    if not res:
        return jsonify({'Error': 'Error occured while getting data from GitHub.'}), 400

    while res.status == 200:
        if 'Link' in res.headers:
            req_str = get_next_link(res.headers['Link'])

        json0 = json.loads(res.read().decode('utf8'))
        json_items = json0['items']
        for x in json_items:
            _out += (filter_row(x), )
        break
        if req_str:
            req = urllib.request.Request(req_str)
            req.add_header('Authorization', 'Bearer {}'.format(session['github_token'][0]))
            res = urllib.request.urlopen(req)
        else:
            break
        if res:
            print(req_str, res.status, len(_out))

    return jsonify(_out), 200


@github.tokengetter
def get_github_oauth_token():
    return session.get('github_token')

@app.route('/')
def index():
    if 'github_token' in session:
        return jsonify({'Info': 'use /task endpoint to get data.'})
    return redirect(url_for('login'))


@app.route('/login')
def login():
    return github.authorize(callback='http://localhost:8080/github_callback', external=True)


@app.route('/logout')
def logout():
    session.pop('github_token', None)
    return jsonify({'Info': 'Logout success'}), 200


@app.route('/github_callback')
def authorized():
    resp = github.authorized_response()
    if resp is None or resp.get('access_token') is None:
        return 'Access denied: reason=%s error=%s resp=%s' % (
            request.args['error'],
            request.args['error_description'],
            resp
        )
    session['github_token'] = (resp['access_token'], '')
    print('New token:', session['github_token'])
    return redirect('/')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
    main()
